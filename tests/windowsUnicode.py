import subprocess
import sys
from pathlib import Path
import json

baseDir = Path(__file__).parent.parent  # repo root
testDir = baseDir / "testDir"
unicodeDir = testDir / "source" / "unicode"
unicodeFilePaths = [str(_.absolute().as_posix()) for _ in unicodeDir.iterdir()]
argFilePath = testDir / "argfile.txt"
outFilePath = testDir / "output.json"

# Create argfile
encoding = "UTF8"
with open(argFilePath, "wb") as fil:
    fil.write(b"\n".join([_.encode(encoding.lower()) for _ in unicodeFilePaths]))

# Run exiftool
# args = ["exiftool", "-j", "-@", f'"{argFilePath.absolute()}"', "-charset", f"filename={encoding}", ">", f'"{outFilePath.as_posix()}"']
# shellArgs = ["exiftool", "-j", "-@", f'"{argFilePath.absolute()}"', "-charset", f"filename={encoding}", ">", f'"{outFilePath.as_posix()}"']
args = ["exiftool", "-j", "-charset", f"filename={encoding}", "-@", argFilePath.absolute().as_posix()]
# exe = subprocess.call(" ".join(shellArgs), shell=True)
exe = subprocess.call(args, stdout=open(outFilePath, "wb"))
print(" ".join(args))


# Read results from file
content = {}
try:
    with open(outFilePath, "rb") as fil:
        content = json.load(fil)
except FileNotFoundError as err:
    print(err)

# Check if results include all source file paths
sourceFilePaths = {_: False for _ in unicodeFilePaths}

for metadata in content:
    filePath = metadata.get("SourceFile")
    if filePath not in sourceFilePaths:
        print("Not found in source files: %s", filePath)
        continue
    sourceFilePaths[filePath] = True

print("===== RESULTS =====")
print(f"All source files have results: {all(sourceFilePaths.values())}")


# Clean up
inp = input("Press a key to clean up")
for _ in [argFilePath, outFilePath]:
    try:
        _.unlink()
    except FileNotFoundError:
        pass
