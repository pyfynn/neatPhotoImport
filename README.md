# neatPhotoImport

<!-- TOC -->

- [neatPhotoImport](#neatphotoimport)
    - [Description](#description)
    - [Requirements](#requirements)
    - [Getting started](#getting-started)
    - [Custom Folder Structure](#custom-folder-structure)
    - [Roadmap](#roadmap)
    - [Authors and acknowledgements](#authors-and-acknowledgements)
    - [License](#license)
    - [Support](#support)
    - [Alternatives](#alternatives)
    - [Behind the Scenes](#behind-the-scenes)
        - [Unicode on Windows](#unicode-on-windows)
            - [Fix A: Use -w output flag](#fix-a-use-w-output-flag)
            - [Fix B: Redirect output to write to file](#fix-b-redirect-output-to-write-to-file)
            - [C: Change Windows PowerShell & CMD encoding to UTF8](#c-change-windows-powershell-&-cmd-encoding-to-utf8)
            - [D: Use UNIX](#d-use-unix)
        - [EXIF Date Metadata](#exif-date-metadata)
            - [Sources](#sources)

<!-- /TOC -->


A simple cross-platform CLI app that copies photos to _date-taken_ folders (`YYYY/YY-MM-DD`)

![neatPhotoImport_fileExplorer.png](./docs/neatPhotoImport_fileExplorer.png)

![neatPhotoImport_example.png](./docs/neatPhotoImport_example.png)


## Description

I didn't want to rely on Lightroom or Darktable's import process to copy photos from the memory card to the right place on my HDD. So I wrote this to keep the naming consistent.

The app is currently a simple CLI using Python and exiftool. It reads the metadata of the photos in the specified source folder and copies the photos to the target folder with this pattern:

`${targetFolder}/YYYY/YYYY-MM-DD/${fileName}`

Which translates to:

`/path/to/target/folder/2021/2021-09-24/nameOfPhoto.ext`

It's a very simple but fast solution that works well for organizing photos without a photo library program.

Please use at your own risk.

## Requirements

- [ExifTool](https://exiftool.org)
- `Python >=3.8`
- For Python module dependencies see [requirements.txt](./requirements.txt)

## Getting started


- Install [ExifTool](https://exiftool.org)
- Install `>=Python3.8` (lower versions may work, but I've used 3.8 so that should definitely work)
- Set up the working environment
  ```shell
  # Clone the repository
  git clone git@gitlab.com:pyfynn/neatPhotoImport.git
  cd neatPhotoImport
  # Setup virtualenv and install dependencies
  python -m venv venv
  # Activate venv on...
  # ...UNIX
  source venv/bin/activate
  # ...Windows Command Prompt
  ./venv/Scripts/Activate.bat
  # ...Windows PowerShell
  ./venv/Scripts/Activate.ps1
  # Install dependencies
  pip install --upgrade pip
  pip install -r requirements.txt
  ```
- Launch the program with `python ./launch.py`


## Custom Folder Structure

There's limited support for using custom tokens to specify an output-folder structure.

```
 Valid tokens for custom pattern:
  %Y    : Year: 0001, 0002, …, 2013, 2014, …, 9998, 9999
  %m    : Month: Month as a zero-padded decimal number. 01, 02, …, 12
  %d    : Day: Day of the month as a zero-padded decimal number. 01, 02, …, 31
  -     : Dash: A dash. Useful as a visual separator
  /     : Path Separator: The symbol used to separate a path's dirs. /some/pat
```

## Roadmap

- :white_check_mark: Support for moving instead of copying (useful for re-organizing)
- Delete empty source folders (also good for re-organizing)
- Support for more time formats (hour, minute, second etc.)
- Support for custom text in target pattern
- Better CLI experience
  - Interactivety with https://github.com/CITGuru/PyInquirer
  - ASCII art with https://github.com/pwaller/pyfiglet
  - Visual progress bars https://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console
- An actual GUI maybe :sparkles:

## Authors and acknowledgements

Thanks to _Phil Harvey_ for making the excellent [ExifTool](https://exiftool.org/)

Thanks to [@sylikc](https://github.com/sylikc) for the [PyExifTool](https://github.com/sylikc/pyexiftool) python module, which was a great inspiration and study material for building suprocess calls

## License

[GNU General Public License v3.0](./LICENSE)

## Support

If you found a bug or have a feature request, please use the issues page to submit it.

If you like this tool and want to help me maintain it and others like it consider to:

<a href="https://www.buymeacoffee.com/fynn" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png" alt="Buy Me A Coffee" style="height: 60px !important;width: 217px !important;" ></a>


## Alternatives

If this tool is not what you were looking for, here are some other options to consider:

- [RapidPhotoDownloader](https://damonlynch.net/rapid/)
- [Photo Move 2](https://www.mjbpix.com/automatically-move-photos-to-directories-or-folders-based-on-exif-date/)
- [Advanced Renamer](https://www.advancedrenamer.com/)
- [DigiKam](https://www.digikam.org/)

---

## Behind the Scenes

I'm using this section to write about some of the roadblocks I encountered while making this tool. May it help you understand some of the weirdness and possibly help you with your own project :heart:

### Unicode on Windows

On Windows I've had quite some trouble with file paths containing unicode characters such as `ùñÍċøĐè`. This is an issue known by `exiftool` and has been discussed in many places in its forums and documentation. Some examples:

- https://exiftool.org/exiftool_pod.html#WINDOWS-UNICODE-FILE-NAMES
- https://exiftool.org/forum/index.php?topic=8438.0
- https://exiftool.org/forum/index.php?topic=2677.0
- https://exiftool.org/forum/index.php?topic=2315.0

I've tried some different methods (see below) to resolve this. But unicode in windows Terminals (cmd and powershell) is simply tricky business. Here's some links to posts that I've stumbled upon while researching this problem:

- Subprocess command encoding https://stackoverflow.com/a/58523012
- 

#### Fix A: Use -w output flag

**Command:** `exiftool -j -w output.txt -@ argfile.txt -charset filename=UTF8 -j`

**Pro:** Official built-in method supported by exiftool

**Con:** Produces lots of sidecar files, which can be I/O heavy and leave garbage all over the place if something should fail before we can delete them again

- Write all filePaths (encoded to utf) to `argfile.txt` in binary mode (`open(filePath, 'wb')`)
- Pass an argfile to exiftool with `-@ argfile.txt`
- Define the `-charset filename=FILEENCODING` (preferably utf8)
- Make exiftool write the results to an output file `-w outputfile.txt`
  - Please note that it generates these for each file in the argfile
- `exiftool -w "_output.json" -@ argfile.txt -charset filename=UTF8 -j`
- Read the results from each output file
- Delete the argfile and all output files

#### Fix B: Redirect output to write to file

(**B** is for **b**est)

**Command:** `exiftool -j -@ argfile.txt -charset filename=UTF8 > output.txt`

**Pro:** Only creates 1 output file, which makes parsing and cleaning up easy. Not writing directly to stdin (which can cause deadlocks as stated by the [python subprocess docs](https://docs.python.org/3.8/library/subprocess.html))

**Con:** Makes threading risky and prone to race-conditions. I'd consider this a minor downside because threading isn't necessary unless you have many thousands of files or you're _really_ impatient. Another downside (for now) is that there's no elegant way to use the `-stay_open` flag because the delay between calling exiftool and the results being written to the file is unpredictable enough that it's unreliable. So for now i've settled on just calling up a new instance for each request. It doesn't make much of a difference because I'm collecting _all_ filePaths first, construct the argfile and then run the command just once.

**Note**: Powershell uses 'UTF-16LE' as default encoding. cmd.exe uses UTF8 per default. :shrug: This shouldn't matter because we read and write files in binary mode.

- Write all filePaths to `argfile.txt` encoded to UTF8
- Pass an argfile to exiftool with `-@ argfile.txt`. See syntax [in docs](https://exiftool.org/exiftool_pod.html#ARGFILE)
- Redirect output to temporary file at the end of the exiftool+options command: `> output.txt`
- Open temp file with UTF8 encoding and parse content
- Delete argfile and temp file

#### C: Change Windows PowerShell & CMD encoding to UTF8

**Command:** `exiftool -j "/path/to/_ùnicøde-pråblæms.jpg"`

**Pro:** Removes the need for writing results to file. We can just read it off the console like on UNIX

**Con:** Requires the user to make system changes, which is a big meh

- Read [this](https://stackoverflow.com/a/40098904) lengthy StackOverflow answer to see how
- And [this](https://markw.dev/unicode_powershell/) unrelated but relevant blog post about unicode in powershell

#### D: Use UNIX

Or use a UNIX based system :wink: - seriously it just works there


### EXIF Date Metadata

According to the EXIF specification the most reliable date information for when a file was actually created is either `DateTimeOriginal` or `DateTimeDigitized` (shown as `CreateDate`).

![neatPhotoImport_exifDateSpecs.png](docs/neatPhotoImport_exifDateSpecs.png)

**neatPhotoImport** checks those two fields - in the above order, using the first one found - to get a file's date.

#### Sources

- exiftool EXIF tags overview https://www.exiftool.org/TagNames/EXIF.html
- EXIF specification (from exiftool docs) https://web.archive.org/web/20190624045241if_/http://www.cipa.jp:80/std/documents/e/DC-008-Translation-2019-E.pdf
- EXIF specification (from exif site) https://www.exif.org/Exif2-2.PDF
