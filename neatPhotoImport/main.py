import os
import json
import tempfile
from abc import ABC, abstractstaticmethod
import argparse
import re
import shutil
import subprocess
import sys
import time
import datetime
from dataclasses import dataclass
from pathlib import Path
from typing import Callable, Iterable, List, Mapping, Dict, Type, overload, Any, Generator
import neatlog


# ========================================
# GLOBAL VARS
# ========================================
LOG = neatlog.getLogger(Path(__file__).stem)
LOG.setLevel('debug')
DEFAULT_DATE_FORMAT: str = "%Y:%m:%d %H:%M:%S"
DEFAULT_EXIF_DATE_TAGS: str = (
    "DateTimeOriginal",
    "CreateDate",
)
DEFAULT_PATTERNS: Iterable[str] = (
    "%Y/%Y-%m-%d",
    "%Y/%m-%d",
    "%Y/%m",
)
DEFAULT_PATTERN: str = DEFAULT_PATTERNS[0]
DEFAULT_FILEOPMODE: "FileOpMode" = None


# ========================================
# ERRORS
# ========================================
class NeatPhotoImportError(Exception):
    pass


class DateParseError(NeatPhotoImportError):
    pass


class PathParseError(NeatPhotoImportError):
    pass


class PatternParseError(NeatPhotoImportError):
    pass


class PatternInvalidError(NeatPhotoImportError):
    pass


class UserInputInvalidError(NeatPhotoImportError):
    pass


# ========================================
# TOKENS
# ========================================
@dataclass
class Token:
    name: str = ""  # eg. "Target Dir"
    description: str = ""  # eg. "Path to the target folder"
    data: str = ""  # eg. "${targetDir}"


@dataclass
class Token_TargetDir(Token):
    name: str = "Target Dir"
    description: str = "Absolute path to the target dir"
    data: str = "${targetDir}"


@dataclass
class Token_FileName(Token):
    name: str = "File Name"
    description: str = "Name of the file (with extension) currently being processed"
    data: str = "${fileName}"


@dataclass
class Token_Year(Token):
    name: str = "Year"
    description: str = "0001, 0002, …, 2013, 2014, …, 9998, 9999"
    data: str = "%Y"


@dataclass
class Token_Month(Token):
    name: str = "Month"
    description: str = "Month as a zero-padded decimal number. 01, 02, …, 12"
    data: str = "%m"


@dataclass
class Token_Day(Token):
    name: str = "Day"
    description: str = "Day of the month as a zero-padded decimal number. 01, 02, …, 31"
    data: str = "%d"


@dataclass
class Token_Dash(Token):
    name: str = "Dash"
    description: str = "A dash. Useful as a visual separator"
    data: str = "-"


@dataclass
class Token_PathSeparator(Token):
    name: str = "Path Separator"
    description: str = "The symbol used to separate a path's dirs. /some/path"
    data: str = "/"


# ========================================
# GET USER INPUT & VALUES
# ========================================
def getUserConfirmation(msg: str, gui=False):
    """Returns True if user confirms, False if

    Args:
        msg (str): Promt for the user
        gui (bool): False: CLI mode, True: GUI mode
    """
    if gui:
        raise NotImplementedError("GUI mode is not implemented")
    while True:
        userInput = input(f"{msg} [y/n]")
        if userInput.lower() == "y":
            return True
        elif userInput.lower() == "n":
            return False


def getUserInput(msg: str, validationFunction: Callable):
    """Prompt user to enter some data. Verify it with the validationFunction

    Args:
        msg (str): Promt to user
        validationFunction (Callable): Checks the user's input. Should return None: try again, False: cancel, True: return

    Returns:
        bool: True if valid input, False if not
    """
    while True:
        userInput = input(msg)
        validation = validationFunction(userInput)
        if not validation:
            # Try again until validation returns truthy result
            continue
        return userInput


@overload
def getUserChoice(msg: str, choices: Dict[str, Any] = {}) -> Any:
    pass


@overload
def getUserChoice(msg: str, choices: List[str] = []) -> str:
    pass


def getUserChoice(msg: str, choices=[]):
    display = f"{msg}\n"
    choiceNames = choices
    choiceValues = choices
    if isinstance(choices, dict):
        choiceNames = list(choices.keys())
        choiceValues = list(choices.values())
    indent = " " * 2
    for index, choiceName in enumerate(choiceNames):
        display += f"{indent}{index + 1}:  {choiceName}\n"
    display += indent

    def validateIndex(userInput: str):
        # Cancel if not a number
        if not userInput.isnumeric():
            LOG.error("Invalid choice. Must be a number present in the list")
            return None
        # Cancel if out of range
        userIndex = int(userInput) - 1
        if userIndex < 0 or userIndex > len(choices) - 1:
            LOG.error("Invalid choice. Must be a number present in the list")
            return None
        # Return index
        return True
    while True:
        userInput = input(display)
        if not validateIndex(userInput):
            continue
        choice = choiceValues[int(userInput) - 1]
        return choice


def runUserFileOpModeTutorial() -> Type["FileOpMode"]:
    """Asks the user to pick a FileOpMode from a predefined list

    Returns:
        FileOpMode: A FileOpMode
    """
    msg = "Choose a fileOp mode: "
    fileOpModeNames = [FileOpMode_Copy.name, FileOpMode_Move.name]
    fileOpModeName = getUserChoice(msg, choices=fileOpModeNames)
    fileOpMode = getFileOpModeByName(fileOpModeName)
    return fileOpMode


def sanitizeUserPattern(pattern: str = None):
    """Removes illegal characters from the user pattern and returns the result

    Args:
        pattern (str, optional): [description]. Defaults to None.
    """
    regExp = r"^\/|\/$"
    return re.sub(regExp, "", pattern)


def validateUserPattern(pattern: str = None):
    """Raises PatternInvalidError if pattern is invalid and PatternParseError if pattern couldn't be parsed

    Pattern must follow these rules:
    - May only contain valid datetime.strftime Format Codes, forward slashes (https://docs.python.org/3/library/datetime.html#strftime-and-strptime-format-codes)
    - May NOT contain any Token_TargetDir.data or Token_FileName.data

    Args:
        pattern (str, optional): [description]. Defaults to None.
    """
    # To validate the pattern go by deduction: Remove all default Token's data.
    # If anything is left in the string, it means it contains invalid data
    remainingPattern = pattern
    for token in [Token_Year, Token_Month, Token_Day, Token_Dash, Token_PathSeparator]:
        remainingPattern = remainingPattern.replace(token.data, "")
    # TODO: Do better checking with regEx. Consider windows slashes, too or sanitize before?
    if remainingPattern:
        raise PatternInvalidError(f"Pattern '{pattern}' contains invalid tokens '{remainingPattern}'")
    return True


def isUserPatternValid(pattern: str = None):
    try:
        validateUserPattern(pattern)
    except PatternInvalidError as err:
        LOG.error(str(err))
        return False
    return True


def makeTargetPattern(userPattern: str = None) -> str:
    """Build the target pattern using the specified userPattern. If None is specified use the DEFAULT_PATTERN

    Args:
        userPattern (str, optional): A user pattern. Defaults to None.

    Returns:
        str: A target pattern
    """
    if userPattern is None:
        userPattern = DEFAULT_PATTERN
    validateUserPattern(userPattern)
    postSep = Token_PathSeparator.data if userPattern else ""
    return f"{Token_TargetDir.data}{Token_PathSeparator.data}{userPattern}{postSep}{Token_FileName.data}"


def runUserPatternTutorial() -> str:
    """Asks the user for a valid pattern and validates it with isUserPatternValid before returning

    Returns:
        str: A valid user pattern
    """
    msg = "\n Valid tokens for custom pattern:"
    for _ in [Token_Year, Token_Month, Token_Day, Token_Dash, Token_PathSeparator]:
        msg += "\n  "
        msg += f"{_.data}".ljust(6, " ")
        msg += f": {_.name}".ljust(5, " ")
        msg += f": {_.description}"

    def tmpValidate(pattern: str):
        """Returns None if pattern is invalid to let the user try again instead of quitting the program

        Args:
            pattern (str): The user specified patterh

        Returns:
            Union[bool, None]: True if valid, None if not
        """
        # Confirm with user to use empty pattern
        if pattern == "":
            if getUserConfirmation(f"Pattern '{pattern}' appears to be empty. Are you sure you want to use this?"):
                return True
            else:
                return None
        if not isUserPatternValid(pattern):
            return None
        return True

    print(msg)
    return getUserInput("Specify a pattern:", tmpValidate)


def validatePattern(pattern: str = None):
    """Raises PatternInvalidError if pattern is invalid and PatternParseError if pattern couldn't be parsed
    Pattern must follow these rules:
    - May only contain valid datetime.strftime Format Codes, forward slashes (https://docs.python.org/3/library/datetime.html#strftime-and-strptime-format-codes)
    - Must contain only 1 Token_TargetDir.data and at least 1 Token_FileName.data

    Args:
        pattern (str, optional): A valid pattern.. Defaults to None.
    """
    startReg = fr"\{Token_TargetDir.data}"
    pathSep = fr"\{Token_PathSeparator.data}{{1}}"
    userTokens = fr"[{Token_Year.data},{Token_Month.data},{Token_Day.data},{pathSep},{Token_Dash.data}]*"
    endReg = fr"\{Token_FileName.data}"
    regExp = fr"{startReg}{pathSep}{userTokens}{pathSep}{endReg}"
    LOG.debug(f"Matching pattern {pattern} against {regExp}")
    if not re.match(regExp, pattern):
        raise PatternInvalidError(f"Pattern '{pattern}' doesn't match '{regExp}'")
    LOG.debug("Checking for duplicate path separators")
    dupExp = r"[^\/]+(\/{2,})[^\/]*"
    if re.match(dupExp, pattern):
        raise PatternInvalidError(f"Pattern '{pattern}' contains double path separators '{Token_PathSeparator.data}'")
    return True


def interpolateTimeStamp(dateTimeObj: datetime.datetime, format: str) -> str:
    """Turns a datetime object into a string

    Args:
        dateTimeObj (datetime.datetime): datetime object
        format (str): A valid strftime format string

    Returns:
        str: A datetime string
    """
    return dateTimeObj.strftime(format)


def interpolatePatternToPath(inpPattern: str, dateTimeObj: datetime.datetime = None, targetDir: Path = None, fileName: str = None) -> Path:
    """Replace the inpPattern's tokens with the specified inputs

    Args:
        inpPattern (str): A string with valid strftime placeholders and Tokens
        dateTimeObj (datetime.datetime, optional): A datetime object. Defaults to None.
        targetDir (Path, optional): Path to the target directory. Defaults to None.
        fileName (str, optional): Name of the source file. Defaults to None.

    Returns:
        [type]: [description]
    """
    outPattern = inpPattern
    if dateTimeObj is not None:
        outPattern = interpolateTimeStamp(dateTimeObj, inpPattern)
    if targetDir is not None:
        outPattern = outPattern.replace(Token_TargetDir.data, str(targetDir.as_posix()))
    if fileName is not None:
        outPattern = outPattern.replace(Token_FileName.data, fileName)
    filePath = Path(outPattern)
    return filePath


def sanitizeInputPath(inpPath: str) -> str:
    return inpPath.strip("'").strip('"')


def validateInputPath(inpPath: str) -> str:
    return validateFolderExistence(sanitizeInputPath(inpPath))


def validateFileExtension(fileExtension: str) -> bool:
    if not fileExtension.strip(" "):
        raise UserInputInvalidError("File extension can't be empty")
    return True


def isFileExtensionValid(fileExtension: str) -> bool:
    try:
        validateFileExtension(fileExtension)
    except UserInputInvalidError as err:
        LOG.error(err)
        return False
    return True


def buildRegExp(fileExtension: str = None):
    """Returns valid regExp with the specified arguments

    Args:
        fileExtension (str, optional): The file extension - a match needs to be found at the end of the string. Defaults to None.

    Returns:
        str: A regExp string
    """
    return fr"(.*)(\.{fileExtension.strip('.')}$)"


def sanitizeRegExp(regExp: str):
    return regExp.strip('"').strip("'")


def validateRegExp(regExp: str):
    if not regExp.strip(" "):
        raise UserInputInvalidError("File extension can't be empty")
    return True


def isRegExpValid(regExp: str):
    try:
        validateRegExp(regExp)
    except UserInputInvalidError as err:
        LOG.error(err)
        return False
    return True


# ========================================
# METADATA
# ========================================
def getExifExecutable() -> str:
    exifExe = "exiftool"
    if sys.platform == 'win32':
        exifExe = "exiftool.exe"
    return exifExe


def getArgFilePath() -> Path:
    tmpDir = Path(tempfile.gettempdir())
    argFileName = "neatPhotoImport_argFile"
    argFilePath = tmpDir / argFileName
    return argFilePath


def getOutputFilePath() -> Path:
    tmpDir = Path(tempfile.gettempdir())
    tmpFileName = "neatPhotoImport_metadata_file.json"
    tmpFilePath = tmpDir / tmpFileName
    return tmpFilePath


def getEncoding() -> str:
    return "UTF8"


def getMetadatas(filePaths: List[Path], tags: List[str] = None) -> List[dict]:
    """Returns list of metadata dicts. One dict for each specified filePath

    Args:
        filePaths (List[Path]): File paths
        tags (List[str], optional): List of tags to read from the metadata. Defaults to None.

    Returns:
        List[dict]: List of metadata dicts
    """
    # Cancel if no filePaths specified
    if not filePaths:
        return
    encoding = getEncoding()
    # Create argFile
    argFilePath = getArgFilePath()
    LOG.debug("Writing %s file paths to argFile: %s", len(filePaths), argFilePath)
    with open(argFilePath, "wb") as fil:
        # Create argFile with all filePaths
        stringPaths = [str(_.absolute().as_posix()).encode(encoding) for _ in filePaths]
        content = b"\n".join(stringPaths)
        fil.write(content)
    # Build outputFilePath (delete if one already exists)
    outputFilePath = getOutputFilePath()
    if outputFilePath.exists() and outputFilePath.is_file():
        outputFilePath.unlink()
    # Create exiftool command
    argFilePath = Path(fil.name)
    args = [getExifExecutable(), "-j", "-charset", f"filename={encoding}", "-@", argFilePath.absolute().as_posix()]
    if tags:
        for tag in tags:
            args.append(f"-{tag}")
    exe = subprocess.run(args, stdout=open(outputFilePath, "wb"))
    if exe.stderr:
        LOG.err(exe.stderr)
    if exe.stdout:
        LOG.debug(exe.stdout)
    if not outputFilePath.exists():
        raise FileNotFoundError("Something went wrong. No outputFile has been created")
    metadatas = None
    with open(outputFilePath, "rb") as fil:
        try:
            metadatas = json.load(fil)
        except json.JSONDecodeError as err:
            # Catch JSONDecodeError, which is thrown if none of the filePaths' metadata contain the specified tag(s)
            LOG.exception(err)
    # Clean up
    for _ in argFilePath, outputFilePath:
        if _.exists() and _.is_file():
            _.unlink()
    return metadatas


def getDateObjFromTimeStamp(timeStamp: str, dateFormat=DEFAULT_DATE_FORMAT) -> datetime.datetime:
    """Returns a datetime object from the specified timeStamp

    Args:
        timeStamp (str): A timestamp as a sting. eg. "2021-10-09 21:30:14"
        dateFormat ([type], optional): A valid strp Format string. Eg. "%Y-%m-%d %H:%M:%S". Defaults to DEFAULT_DATE_FORMAT.

    Raises:
        DateParseError: Raised if timestamp couldn't be parsed with the specified dateFormat

    Returns:
        datetime.datetime: datetime object
    """
    try:
        dateTimeObject = datetime.datetime.strptime(timeStamp, dateFormat)
    except ValueError as err:
        msg = f"Failed to parse time stamp '{timeStamp}': {err}"
        raise DateParseError(msg)
    except TypeError as err:
        msg = f"Failed to parse time stamp '{timeStamp}': {err}"
        raise DateParseError(msg)
    else:
        return dateTimeObject


def getDatesTaken(filePaths: List[Path], dateTags=DEFAULT_EXIF_DATE_TAGS, dateFormat=DEFAULT_DATE_FORMAT) -> Mapping[str, datetime.datetime]:
    """Returns a dict with the date for each specified filePath: {filePath: datetime.datetime}. If date can't be found it will be {filePath: None}

    Args:
        filePaths (List[Path]): List of Paths
        dateTag (List[str], optional): Prioritized list of the EXIF tags to look for the date. First found is used. Defaults to DEFAULT_EXIF_DATE_TAGS.
        dateFormat (str, optional): A valid strftime format string. Defaults to DEFAULT_DATE_FORMAT.

    Returns:
        Mapping[str, datetime.datetime]: [description]
    """
    filesAndDates = {}
    metadatas = getMetadatas(filePaths, tags=dateTags)
    if not metadatas:
        LOG.warning("No metadata found for the %s specified filePaths", len(filePaths))
        return filesAndDates
    for meta in metadatas:
        filePath = meta.get("SourceFile")
        # Check all provided tags and use the date from the first found one
        for tagName in dateTags:
            timeStamp = meta.get(tagName)
            if timeStamp:
                break
        dateObj = None
        if not timeStamp:
            msg = f"Can't find date tag(s) '{dateTags}' in metadata of '{filePath}'"
            LOG.error(msg)
            dateObj = DateParseError(msg)
        else:
            try:
                dateObj = getDateObjFromTimeStamp(timeStamp=timeStamp, dateFormat=dateFormat)
            except DateParseError as err:
                LOG.error(str(err))
                dateObj = DateParseError
        filesAndDates[filePath] = dateObj
    return filesAndDates


# ========================================
# FILE OPS
# ========================================
def makeDirs(filePath: Path = None):
    dirPath = filePath.parent
    if filePath.is_dir():
        dirPath = filePath
    if dirPath.exists():
        return
    LOG.debug("Creating folder: '%s'", dirPath)
    os.makedirs(dirPath)


def copyFile(sourcePath: Path, targetPath: Path):
    shutil.copy(sourcePath, targetPath)


def moveFile(sourcePath: Path, targetPath: Path):
    shutil.move(sourcePath, targetPath)


class FileOpMode(ABC):
    name: str = ""

    @abstractstaticmethod
    def run(sourcePath: Path, targetPath: Path):
        pass


class FileOpMode_Copy(FileOpMode):
    name = "copy"

    @staticmethod
    def run(sourcePath: Path, targetPath: Path):
        copyFile(sourcePath, targetPath)


class FileOpMode_Move(FileOpMode):
    name = "move"

    @staticmethod
    def run(sourcePath: Path, targetPath: Path):
        moveFile(sourcePath, targetPath)


class FileOp:
    """A class that represents a single file operation (move/copy)
    """
    mode: FileOpMode = None
    makeDirs: bool = True
    sourcePath: Path = None
    targetPath: Path = None

    def __init__(self, fileOpMode: FileOpMode = None, makeDirs: bool = True, sourcePath: Path = None, targetPath: Path = None):
        self.mode = fileOpMode
        self.makeDirs = makeDirs
        self.sourcePath = sourcePath
        self.targetPath = targetPath

    def run(self):
        if self.makeDirs:
            makeDirs(self.targetPath)
        self.mode.run(self.sourcePath, self.targetPath)


def getFileOpModeByName(name: str = None) -> Type[FileOpMode]:
    modes = {
        "copy": FileOpMode_Copy,
        "move": FileOpMode_Move
    }
    return modes.get(name)


def deleteEmptyDirs(sourceDir: Path = None, recursive: bool = False):
    deletedDirs = []
    for root, folders, files in os.walk(sourceDir, topdown=True):
        for entryName in os.listdir(root):
            entryPath = Path(root) / entryName
            if not entryPath.is_dir():
                continue
            for fileName in os.listdir(entryPath):
                if fileName == "desktop.ini":
                    filePath = entryPath / fileName
                    LOG.info("Deleting: %s" % fileName)
                    os.remove(filePath)
            fileNames = os.listdir(entryPath)
            if fileNames:
                continue
            relPath = entryPath.relative_to(sourceDir)
            LOG.debug("Deleting empty folder: %s", relPath)
            deletedDirs.append(entryPath)
            os.rmdir(entryPath)
        if not recursive:
            break
    return deletedDirs


def getFilePaths(sourceDir: Path = None, recursive: bool = True, matchRegEx: str = None) -> Generator[Path, None, None]:
    """Yields filePaths (matching the (optional) filter) inside the sourceDir

    Args:
        sourceDir (Path, optional): The source directory. Defaults to None.
        recursive (bool, optional): Dive into subdirs. Defaults to True.
        matchRegEx (str, optional): If specified only fileNames matching this regExp are yielded. Defaults to None.

    Yields:
        Generator[Path, None, None]: Path objects
    """
    for root, _, files in os.walk(sourceDir, ):
        for fileName in files:
            # Skip Path if it's invalid
            if matchRegEx and not re.match(matchRegEx, fileName):
                continue
            filePath = Path(root, fileName)
            yield filePath
        if not recursive:
            break


# ========================================
# MAIN
# ========================================
class Main():
    def __init__(self, targetPattern: str = "", fileOpMode: FileOpMode = None, sourceDir: str = "", targetDir: str = "", deleteEmptySourceDirs: bool = False, dryMode: bool = False, regExp: str = None):
        self.sourceDir = "" if not sourceDir else Path(sourceDir)
        self.targetDir = "" if not targetDir else Path(targetDir)
        self.targetPattern = "" if not targetPattern else targetPattern
        self.regExp = regExp
        self.dryMode = dryMode
        self.deleteEmptySourceDirs = deleteEmptySourceDirs
        self.autoConfirm = False
        self.fileOpMode: FileOpMode = fileOpMode

    def validateOptions(self):
        if not self.sourceDir or not Path(self.sourceDir).exists():
            LOG.error('Source dir doesn\'t exist: "%s"', self.sourceDir)
            return False
        if not self.targetDir or not Path(self.targetDir).exists():
            LOG.error('Target dir doesn\'t exist: "%s"', self.targetDir)
            return False
        # Validate pattern
        patternIsValid = False
        try:
            validatePattern(self.targetPattern)
        except PatternInvalidError as err:
            LOG.error('Invalid target pattern: %s', err)
        except PatternParseError as err:
            LOG.error('Failed to parse pattern: %s', err)
        else:
            patternIsValid = True
        if not self.targetPattern or not patternIsValid:
            return False
        # Validate fileOpMode
        if not self.fileOpMode or not issubclass(self.fileOpMode, FileOpMode):
            LOG.error("Invalid fileOpMode '%s'", self.fileOpMode)
            return False
        return True

    def startImport(self):
        # Cancel if any option is invalid
        if not self.validateOptions():
            return
        msg = f"Will {self.fileOpMode.name} files to target using this pattern: {self.targetPattern}\nDo you want to continue?"
        userConfirmsTargetPattern = getUserConfirmation(msg)
        if self.autoConfirm is False and userConfirmsTargetPattern is False:
            LOG.info("User Cancelled")
            return
        # Build overview of files
        # ----------------------------------------
        sTime = time.time()
        filesAndTargets = {}
        filesAndErrors = {}
        # Get all file paths
        LOG.info("Indexing files...")
        filePaths = list(getFilePaths(sourceDir=self.sourceDir, recursive=True, matchRegEx=self.regExp))
        # Get all time stamps
        LOG.info("Fetching time stamps (this may take a while)...")
        filesAndDates = getDatesTaken(filePaths)
        # Build filesAndTargets and filter files with errors
        LOG.info("Building target paths...")
        for filePathStr, date in filesAndDates.items():
            filePath = Path(filePathStr)
            if isinstance(date, DateParseError):
                filesAndErrors[filePath] = date
                continue
            targetPath = interpolatePatternToPath(self.targetPattern, dateTimeObj=date, targetDir=self.targetDir, fileName=filePath.name)
            if targetPath.exists():
                msg = f"File already exists at target: {targetPath}"
                LOG.warning(msg)
                filesAndErrors[filePath] = FileExistsError(msg)
                continue
            filesAndTargets[filePath] = targetPath
        eTime = time.time() - sTime
        LOG.info(f"Prepped {len(filesAndTargets)} files in {eTime:0.2f}s")
        # List errors
        # ----------------------------------------
        if filesAndErrors:
            LOG.error(f"{len(filesAndErrors)} errors occured while indexing. See logs above for details.")
            if not filesAndTargets:
                LOG.info("No valid files left to %s.", self.fileOpMode.name)
                return
            userConfirms = getUserConfirmation(f"Do you wan't to proceed to {self.fileOpMode.name} the remaining {len(filesAndTargets)} files?")
            if not userConfirms:
                LOG.info("User cancelled")
                return
        # Process Files
        # ----------------------------------------
        sTime = time.time()
        processedFiles = {}
        sourcePath: Path
        for index, sourcePath in enumerate(filesAndTargets.keys()):
            targetPath: Path = filesAndTargets.get(sourcePath)
            LOG.debug(f"Copying  {index + 1}/{len(filesAndTargets)} ${{SourcePath}}/{sourcePath.relative_to(self.sourceDir).as_posix()} > {Token_TargetDir.data}/{targetPath.relative_to(self.targetDir).as_posix()}")
            processedFiles[sourcePath] = targetPath
            fileOp = FileOp(fileOpMode=self.fileOpMode, makeDirs=True, sourcePath=sourcePath, targetPath=targetPath)
            if not self.dryMode:
                fileOp.run()
        eTime = time.time() - sTime
        LOG.info(f"Successfully did {self.fileOpMode.name} {len(processedFiles)} files in {eTime:0.2f}s from '{self.sourceDir}' to '{self.targetDir}'")


def validateFolderExistence(dirPath: str):
    if not dirPath or not Path(dirPath).exists():
        LOG.error("No such folder: '%s'", dirPath)
        return False
    return True


def mainCLI():
    # Set up argParser
    argParser: argparse.ArgumentParser = argparse.ArgumentParser(description='A simple CLI app that imports photos to _date-taken_ folders')
    argParser.add_argument('-i', '--interactive', action="store_true", default=False, help="If True, guide user through setup step-by-step. If False all required arguments have to be specified when launching")
    obligatoryPaths = True
    if '-i' in sys.argv or '--interactive' in sys.argv:
        obligatoryPaths = False
    argParser.add_argument('-s', '--source', required=obligatoryPaths, action="store", type=str, help="Path to the source folder")
    argParser.add_argument('-t', '--target', required=obligatoryPaths, action="store", type=str, help="Path to the target (destination) folder")
    argParser.add_argument('--pattern', nargs="?", action="store", type=str, help="A custom target pattern")
    argParser.add_argument('--fileop', nargs="?", choices=['copy', 'move'], default="copy", help="Which file operation to use")
    argParser.add_argument('--regExp', action='store', type=str, help="Specify a regular expression that file names have to match in order to be processed (eg. '^((?!json).)*$' to match all except .json). If specified the --extension flag is ignored")
    argParser.add_argument('--extension', nargs="?", action="store", type=str, help="Only process files with this extension (eg. 'tiff', 'jpg', 'CR2')")
    argParser.add_argument('--dry', action="store_true", help="Dry run. Don't actually copy/move anything, just print to the console what will happen.")
    args = argParser.parse_args()
    interactive = args.interactive
    # Get sourceDir
    sourceDir = None if not args.source else args.source
    if not sourceDir and interactive:
        sourceDir = getUserInput("Please specify a source folder:", validateInputPath)
    sourceDir = sanitizeInputPath(sourceDir)
    # Get targetDir
    targetDir = None if not args.target else args.target
    if not targetDir and interactive:
        targetDir = getUserInput("Please specify a target folder:", validateInputPath)
    if not targetDir:
        LOG.info("No target folder provided. Exiting.")
        return
    targetDir = sanitizeInputPath(targetDir)
    # Get custom pattern
    userPattern = None if not args.pattern else args.pattern
    if not userPattern and interactive:
        userPattern = getUserChoice("Pick a target pattern", list(DEFAULT_PATTERNS) + ["custom"])
        if userPattern not in DEFAULT_PATTERNS:
            userPattern = runUserPatternTutorial()
    if userPattern:
        userPattern = sanitizeUserPattern(userPattern)
    try:
        pattern = makeTargetPattern(userPattern=userPattern)
    except PatternInvalidError as err:
        LOG.error("An error occured while building pattern: %s", err)
        return
    # Get filter
    regExp = None if not args.regExp else args.regExp
    extension = None if not args.extension else args.extension
    if not regExp and not extension and interactive:
        choices = {
            "All": False,
            "Specify extension": 1,
            "Specify regExp": 2
        }
        userChoiceAffect = getUserChoice("Which files should be affected?", choices=choices)
        if userChoiceAffect == 1:
            extension = getUserInput("Specify a file extension: ", isFileExtensionValid)
            regExp = buildRegExp(fileExtension=extension)
        elif userChoiceAffect == 2:
            regExp = getUserInput("Specify a regular expression: ", isRegExpValid)
    if regExp:
        regExp = sanitizeRegExp(regExp)
    elif extension:
        regExp = buildRegExp(fileExtension=extension)
    # Get fileOpMode
    fileOpModeName = None if not args.fileop else args.fileop
    fileOpMode = getFileOpModeByName(fileOpModeName)
    if not fileOpMode and interactive:
        if getUserConfirmation(f"Would you like to use a different fileOp? (current: {DEFAULT_FILEOPMODE.name})?"):
            fileOpMode = runUserFileOpModeTutorial()
        else:
            fileOpMode = DEFAULT_FILEOPMODE
    # Get dry mode
    dryMode = args.dry
    print(f"Dry mode: {dryMode}")
    print(f"sourceDir: {sourceDir}")
    print(f"targetDir: {targetDir}")
    main = Main(targetPattern=pattern, fileOpMode=fileOpMode, sourceDir=sourceDir, targetDir=targetDir, dryMode=dryMode, regExp=regExp)
    main.startImport()


DEFAULT_FILEOPMODE = FileOpMode_Copy
