import launch
import os
from pathlib import Path
import neatPhotoImport as npi
import neatlog

LOG = neatlog.getLogger(__file__)
LOG.setLevel('debug')

baseDir = Path(__file__).parent.parent  # repo root
testDir = baseDir / "testDir"
sourceDir = testDir / "source"
filePaths = []


for root, folders, files in os.walk(sourceDir):
    for fileName in files:
        filePaths.append(Path(root) / fileName)


metadatas = npi.main.getMetadatas(filePaths=filePaths, tags=["DateTimeOriginal"])
for metadata in metadatas:
    sourceFilePath = metadata.get("SourceFile", None)
    if not sourceFilePath:
        LOG.error("Faulty SourceFile path: %s", sourceFilePath)
        continue
    print(metadata)
