import sys
import os
import exiftool
import launch
from pathlib import Path
from neatPhotoImport import main as npi
import neatlog


LOG = neatlog.getLogger(__file__)
LOG.setLevel("debug")

baseDir = Path(__file__).parent.parent  # repo root
testDir = baseDir / "testDir" / "source"
unicodeDir = testDir / "unicode"

# filePaths = [
#     testDir / "unicode" / "_ùnicøde-pråblæms.jpg"
#     # "P:/photos/other/2007/2007-03-10/koncentreret nørd.JPG",  # invalid
#     # "C:/Users/fynn/Documents/Development/gitLab/pyfynn/neatPhotoImport/testDir/source/sub1/_MG0769_sm.jpg",  # valid
# ]

filePaths = []
for _ in os.listdir(unicodeDir):
    filePaths.append(unicodeDir / _)


unicodePath = str(filePaths[0].as_posix())
bytePath = bytes(unicodePath.encode('utf-8'))
keys = npi.DEFAULT_EXIF_DATE_TAGS


def fullCall():
    """Requires you to make a new subprocess.Popen call everytime. This may be slower than keeping the smae exiftool instance open
    """
    import subprocess
    import json
    # https://stackoverflow.com/questions/33283603/python-popen-communicate-str-encodeencoding-utf-8-errors-ignore-cr
    # https://stackoverflow.com/questions/3810302/python-unicode-popen-or-popen-error-reading-unicode  windows terminal uses cp437 encoding
    print(unicodePath)
    print(bytePath)
    # Specifying -charset UTF8 fixes communicate UnicodeDecodeError when stdin=subprocess.PIPE
    exe = subprocess.Popen(["exiftool", "-charset", "UTF8", "-execute", "-j", bytePath], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = exe.communicate()
    outUnicode = out  # .decode("utf-8", errors="ignore") # only decode if "-charset is not defined in exiftool args"
    print(outUnicode)
    print(json.loads(outUnicode))
    print("ERRORS:", err)
    sys.exit(0)


def sameInstance():
    import subprocess
    import os
    import json
    fsEncoding = sys.getfilesystemencoding()
    with open(os.devnull, "w") as devnull:
        startup_info = subprocess.STARTUPINFO()
        startup_info.dwFlags |= 11
        exe = subprocess.Popen(["exiftool", "-stay_open", "True", "-@", "-", "-common_args"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=devnull, startupinfo=startup_info, universal_newlines=True)
    params = (bytePath, )
    cmd_text = b"\n".join((b"-j", ) + params + (b"-execute\n",))
    exe.stdin.write(cmd_text.decode(fsEncoding))
    exe.stdin.flush()
    # out, err = exe.communicate()  # input=bytes(f"-j --execute {safePath}".encode("utf-8"))
    # print(out.decode("utf-8"))

    def oke():
        sentinel = b"{ready}".decode(fsEncoding)
        block_size = 4096
        output = ""
        fd = exe.stdout.fileno()
        while not output[-32:].strip().endswith(sentinel):
            if sys.platform == 'win32':
                # windows does not support select() for anything except sockets
                # https://docs.python.org/3.7/library/select.html
                line = os.read(fd, block_size).decode(fsEncoding, errors="ignore")
                output += line
        return output.strip()[:-len(sentinel)]

    out = oke()
    result = json.loads(out)
    print(result)


def sameInstanceBytes():
    """THIS CAN'T BE DONE ELEGANTLY. It's better to use suprocess.Popen as it's intended in python3: with str, not bytestrings (b"")
    """
    import subprocess
    import os
    import json
    fsEncoding = sys.getfilesystemencoding()
    with open(os.devnull, "w") as devnull:
        startup_info = subprocess.STARTUPINFO()
        startup_info.dwFlags |= 11
        exe = subprocess.Popen(["exiftool", "-stay_open", "True", "-@", "-", "-common_args"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=devnull, startupinfo=startup_info)
    params = (bytePath, )
    cmd_text = b"\n".join((b"-j", ) + params + (b"-execute\n",))
    coooomaaa = cmd_text.replace(b'\r\n', b'\n').replace(b'\r', b'\n')
    print(cmd_text)
    print(coooomaaa)
    exe.stdin.write(coooomaaa)
    exe.stdin.flush()
    exe.stdout.flush()

    # out, err = exe.communicate()  # input=bytes(f"-j --execute {safePath}".encode("utf-8"))
    # print(out.decode("utf-8"))

    def oke():
        sentinel = b"{ready}"
        block_size = 4096
        output = b""
        fd = exe.stdout.fileno()
        while not output[-32:].strip().endswith(sentinel):
            if sys.platform == 'win32':
                # windows does not support select() for anything except sockets
                # https://docs.python.org/3.7/library/select.html
                line = os.read(fd, block_size)
                output += line
        return output.strip()[:-len(sentinel)]

    out = oke().decode(fsEncoding)
    print("out: ", out)
    result = json.loads(out)
    print(result)


def neatImport():
    metadatas = npi.getMetadatas(filePaths, tags=None)
    for metadata in metadatas:
        warning = metadata.get("ExifTool:Warning")
        if warning:
            LOG.warning(warning)
            continue
    return metadatas


# fullCall()
# sameInstance()
# sameInstanceBytes()
neatImport()


sys.exit(0)
