'''
file: ex_main.py
info: Simulate a simple test case of importing files from a source folder to a target folder
      Uses the /testDir folder in this repo as an example
'''
from pathlib import Path
import shutil
import sys
import launch

launch.main()
