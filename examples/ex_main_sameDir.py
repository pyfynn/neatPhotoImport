'''
file: ex_main.py
info: Simulate a simple test case of importing files from a source folder to a target folder
      Uses the /testDir folder in this repo as an example
'''
from pathlib import Path
import shutil
import sys
import launch

baseDir = Path(__file__).parent.parent  # repo root
testDir = baseDir / "testDir"
sourceDir = testDir / "source"
targetDir = testDir / "target"

# Reset target dir
if targetDir.exists():
    shutil.rmtree(targetDir)
# targetDir.mkdir()

# Copy source files into targetDir
shutil.copytree(sourceDir, targetDir)

# Add source dir to args
sys.argv.extend(["--cli"])
sys.argv.extend(["--source", str(targetDir)])
sys.argv.extend(["--target", str(targetDir)])

launch.main()
