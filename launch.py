import sys
# Add ./lib to pythonpath so neatlog can be used. \
# The .env file specifies this for debugging sessions defined in .vscode/launch.json. \
# That's why we're checking here if it's already added
if "./lib" not in sys.path:
    sys.path.append("./lib")
import neatPhotoImport
import neatlog

LOG = neatlog.getLogger("neatPhotoImport")
LOG.setLevel('debug')


def main():
    guiMode = False
    if not guiMode:
        try:
            neatPhotoImport.main.mainCLI()
        except KeyboardInterrupt:
            LOG.info("User Cancelled")
        except OSError as err:
            LOG.exception(err)
            LOG.info("User Cancelled (or something crashed - see log)")
        except EOFError as err:
            LOG.exception(err)
            LOG.info("User Cancelled (or something crashed - see log)")


if __name__ == "__main__":
    main()
